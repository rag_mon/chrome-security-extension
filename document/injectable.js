const EXTENSION_ID = 'cdhcdmffjaemjpbfnhocghfmknilmldf';


const DATA_TYPE_HEADERS = 'headers';
const DATA_TYPE_BODY = 'body';
const DATA_TYPE_URL = 'url';


const MESSAGE_TYPE_GET_URL_PERMISSION = 'get_url_permission';


const contentTests = {
    'ssn' : function(content) {
        // source: 078 05 1120 | 078-05-1120
        return /(ssn)[ ="':\[\]\{\}\\]*(\d{3})[- +]?(\d{2})[- +]?(\d{4})/gmi.test(content);
    },
};

// ---------------------------------------------------------------------------------

function testContent(data, dataType) {
    console.debug('Testing data', data, dataType);
    for (const [testName, testFunc] of Object.entries(contentTests)) {

        // if (testName in ) continue;

        console.debug(`Running content test function "${testName}"...`);
        if (testFunc(data)) {
            console.debug(`Test function "${testName}" found matches`);
            throw {
                dataType : dataType,
                found : testName
            };
        }
    }
}

// ---------------------------------------------------------------------------------

function getPermissionForUrl(url) {
    console.log('getPermissionForUrl', url);
    // chrome.extension.sendMessage({}, function(response) {
    //     //code to initialize my extension
    // });
    // chrome.runtime.sendMessage({
    //     type : MESSAGE_TYPE_GET_URL_PERMISSION,
    //     url : url
    // }, function (response) {
    //     console.log('response from chrome.runtime.sendMessage', response);
    // });
    // chrome.runtime.sendMessage({type: 'get_url_permission'}, (response) => {
    //     console.log('chrome.runtime.sendMessage response', response);
    // });
    // console.log('test');
    // window.postMessage({
    //     type : MESSAGE_TYPE_GET_URL_PERMISSION,
    //     url : url
    // });
}

function showActionPopup(found) {

}

function AjaxHookInternal() {
    const self = this;

    this.oldOpen = XMLHttpRequest.prototype.open;
    this.oldSetRequestHeader = XMLHttpRequest.prototype.setRequestHeader;
    this.oldSend = XMLHttpRequest.prototype.send;
    this.last_url = null;

    XMLHttpRequest.prototype.open = function (method, url, async, user, password) {
        console.debug('XMLHttpRequest.prototype.open proxy call');
        getPermissionForUrl(url);
        // try {
        //     testContent(decodeURI(url), DATA_TYPE_URL);
        // } catch (found) {
        //     console.debug('Found some vulnerable data in URL', url, found);
        // }

        // self.last_url = url;

        // self.oldOpen(this, arguments);
    };
    // XMLHttpRequest.prototype.setRequestHeader = function () {
    //     //
    //     console.debug('XMLHttpRequest.prototype.setRequestHeader');
    // };
//     XMLHttpRequest.prototype.send = function (body) {
//         console.debug('XMLHttpRequest.prototype.send proxy call', body);
//         // var XMLHttpRequestContext = this;
//         // setTimeout(function () {
//         //     self.oldSend.apply(XMLHttpRequestContext, arguments);
//         // }, 3000);
//         // console.debug('body: ', body);
//         // if (body === null) {
//         // } else {
//         //     throw 'Unexcpected body type: ' + (typeof body);
//         // }

//         // if () {
// // 
//         // }

//         try {
//             testContent(JSON.stringify(body), DATA_TYPE_BODY);
//         } catch (found) {
//             console.debug('Found some vulnerable data in requestBody', body, found);
//             //
//         }
//     };
}


// ---------------------------------------------------------------------------------


// const chromeRuntimePort = chrome.runtime.connect(EXTENSION_ID);

// window.addEventListener("message", function(event) {
//     console.log('window.addEventListener', event);
//     // We only accept messages from ourselves
//     if (event.source != window)
//         return;

// //   if (event.data.type && (event.data.type == "FROM_PAGE")) {
// //     console.log("Content script received: " + event.data.text);
// //     chromeRuntimePort.postMessage(event.data.text);
// //   }
//     chrome.runtime.sendMessage(EXTENSION_ID, event.data);
// }, false);




var ajaxHookInternal = new AjaxHookInternal();
console.debug('Internal AJAX hook installed ....');