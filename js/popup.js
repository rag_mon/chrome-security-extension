function getSettingFromDOM() {
    const setting = {};

    $('.setting').each(function () {
        setting[this.name] = this.checked;
    });

    return setting;
}

function storeSetting(setting) {
    console.debug('Store setting', setting);

    chrome.storage.sync.set({
        'setting' : setting
    }, () => {
        chrome.runtime.sendMessage({type: 'setting_changed'});
    });
}

function populateDOMSetting(setting) {
    console.debug('Populate DOM setting', setting);

    for (const [name, value] of Object.entries(setting)) {
        console.debug(`Set DOM setting property`, name, value);

        $(`.setting[name=${name}]`).prop('checked', value);
    }
}

function loadSetting() {
    chrome.storage.sync.get(['setting'], (result) => {
        const setting = result.setting;

        console.debug('Loaded setting', setting);

        populateDOMSetting(setting);
    });
}



$(document).ready(function () {

    console.log('$(document).ready');
    
    loadSetting();

    $('.setting').change(function () {
        const setting = getSettingFromDOM();

        storeSetting(setting);
    });

});