const EXTENSION_NAME = "chrome-security-extension";

const DATA_TYPE_HEADERS = "headers";
const DATA_TYPE_BODY = "requestBody";
const DATA_TYPE_URL = "url";

const trackingRequestTypes = ["xmlhttprequest"];

const contentTests = {
  ssn: function (content) {
    // source: 078 05 1120 | 078-05-1120
    return /(ssn)[ ="':\[\]\{\}\\]*(\d{3})[- ]?(\d{2})[- ]?(\d{4})/gim.test(
      content
    );
  },
  birthday: function (content) {
    // format: MM/DD/YYYY
    // source: 1991-12-01 | 1923/12/01
    return /(birthday|birth)[ ="':\[\]\{\}\\]*(\d{4})[\/-](\d{2})[\/-](\d{2})/gim.test(
      content
    );
  },
  'name': function (content) {
      // source: Dave Jones
      return /(name|firstname|lastname|patronymic|user|username|fullname)[ ="':\[\]\{\}\\]*([a-z]+)/gmi.test(content);
  },
  card: function (content) {
    // source: 6666 6666 6666 6666
    return /(card)[ ="':\[\]\{\}\\]*(\d{4})[ ]?(\d{4})[ ]?(\d{4})[ ]?(\d{4})/gim.test(
      content
    );
  },
  cvv: function (content) {
    // source: 123
    return /(cvv)[ ="':\[\]\{\}\\]*(\d{3})/gim.test(content);
  },
  'user_id': function (content) {
      // source: 69
      return /(id|user|user_id|user-id)[ ="':\[\]\{\}\\]*(\d+)/gmi.test(content);
  },
  password: function (content) {
    // source: 76.23%&r3*2f!28
    return /(password|pass)[ ="':\[\]\{\}\\]*(.+)/gim.test(content);
  },
  'ip': function (content) {
      // source: 127.0.0.1
      // return /(ip|address|ip_address|ip_addr|addr)[ ="':\[\]\{\}\\]*(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/gmi.test(content);
      return /(ip|address|ip_address|ip_addr|addr)[ ="':\[\]\{\}\\]*(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/gmi.test(content);
  }
};

function testContent(data, dataType) {
  console.debug("Testing data", data, dataType);
  const found = [];
  for (const [testName, testFunc] of Object.entries(contentTests)) {
    if (!testEnabled(testName)) continue;

    console.debug(`Running content test function "${testName}"...`);

    if (testFunc(data)) {
      console.debug(`Test function "${testName}" found matches`);
      found.push({
        name: testName,
        dataType: dataType,
      });
    }
  }
  return found;
}

function testEnabled(testName) {
    return globalSetting.setting[testName];
}

// ---------------------------------------------------------------------------------

// const extensionState = {
//     urlPermissionListLoaded : false
// };
let globalSetting = getDefaultSetting();
const db = openDatabase();
let urlPermissionList = [];

// function getReadyState() {
//     let state = true;
//     for (const [name, value] of Object.entries(extensionState)) {
//         state = state && value;
//     }
//     return state;
// }

function getDefaultSetting() {
  return {
    birthday: true,
    card: true,
    cvv: true,
    extension_enabled: true,
    ip: true,
    name: true,
    password: true,
    ssn: true,
    user_id: true,
  };
}

function isExtensionEnabled() {
    return globalSetting.setting.extension_enabled;
}

function openDatabase() {
  return new Promise((resolve, reject) => {
    const openDB = indexedDB.open("common", 1);

    openDB.onupgradeneeded = function (event) {
      console.debug("Common database needed upgrade", event);
      const db = event.target.result;

      if (event.oldVersion < 1) {
        const urlStore = db.createObjectStore("url", { keyPath: "url" });
        const urlIndex = urlStore.createIndex("url", "url", { unique: true });
      }
    };

    openDB.onerror = function (event) {
      console.error("Error loading database", event);

      reject(event);
    };

    openDB.onsuccess = function (event) {
      console.debug("Common database connection established");

      resolve(openDB.result);
    };

    // return openDB;
  });
}

function loadUrlPermissionList() {
  openDatabase().then((db) => {
    const transaction = db.transaction(["url"], "readonly");
    const storeObject = transaction.objectStore("url");

    const storeObjectRequest = storeObject.getAll();

    storeObjectRequest.onsuccess = function (event) {
      urlPermissionList = event.target.result;
      console.debug("Loaded url permissions from database", urlPermissionList);
      // db.close();
    };

    storeObjectRequest.onerror = function (event) {
      console.error("Error loading store object for url permission");
    };
  });
}

function initGlobalSettings() {
  const setting = getDefaultSetting();
  chrome.storage.sync.set({ setting: setting }, function () {
    console.debug("Global setting set", setting);
  });
}

function findUrlRequestPermission(url) {
  return urlPermissionList.find((permission) => permission.url === url);
}

function isAllowedUrlRequest(url) {
  url = prepareUrl(url);

  const permission = findUrlRequestPermission(url);

  if (permission) {
    // flush onetime request permission
    if (!permission.remember) {
      removeUrlPermission(url);
    }

    return permission.allowed;
  }

  return false;
}

function prepareUrl(url) {
  return url.split(/[?#]/)[0].toLowerCase();
}

function askForRequestPermission(url, foundList, tabID) {
  console.debug("Asking for request permission", url, foundList, tabID);

  const permission = addUrlPermission(url);

  chrome.tabs.sendMessage(tabID, {
    type: "ask_for_request_permission",
    url: url,
    foundList: foundList,
  });
}

function makeUrlPermissionObject(url, allowed, remember) {
  return {
    url: prepareUrl(url),
    allowed: allowed,
    remember: remember,
  };
}

function addUrlPermission(url, allowed = false, remember = false) {
  const permission = makeUrlPermissionObject(url, allowed, remember);

  addUrlPermissionToCache(permission);
  addUrlPermissionToDB(permission);

  return permission;
}

function addUrlPermissionToCache(permission) {
  urlPermissionList.push(permission);
}

function addUrlPermissionToDB(permission) {
  openDatabase().then((db) => {
    const transaction = db.transaction(["url"], "readwrite");

    // report on the success of the transaction completing, when everything is done
    transaction.oncomplete = function (event) {
      console.debug("Add url permission transaction complete", event);
    };

    transaction.onerror = function (event) {
      console.error("Add url permission transaction error", event);
    };

    const objectStore = transaction.objectStore("url");

    const objectStoreRequest = objectStore.add(permission);

    objectStoreRequest.onsuccess = function (event) {
      console.debug("Add url permission object store request success", event);
    };
  });
}

function removeUrlPermission(url) {
  removeUrlPermissionFromCache(url);
  removeUrlPermissionFromDB(url);
}

function removeUrlPermissionFromCache(url) {
  const cacheIndex = urlPermissionList.findIndex(
    (element) => element.url.toLowerCase() == url.toLowerCase()
  );
  if (cacheIndex >= 0) {
    urlPermissionList.splice(cacheIndex, 1);
  }
}

function removeUrlPermissionFromDB(url) {
  openDatabase().then((db) => {
    const transaction = db.transaction(["url"], "readwrite");

    // report on the success of the transaction completing, when everything is done
    transaction.oncomplete = function (event) {
      console.debug("Url transaction remove complete", event);
    };

    transaction.onerror = function (event) {
      console.error("Url transaction remove error", event);
    };

    const objectStore = transaction.objectStore("url");

    const objectStoreRequest = objectStore.delete(url);

    objectStoreRequest.onsuccess = function (event) {
      console.debug("Url object store remove request success", event);
    };

    objectStoreRequest.onerror = function (event) {
      console.error("Url object store remove request error", event);
    };
  });
}

function updateUrlPermission(permission) {
  console.debug("updateUrlPermission", permission);

  updateUrlPermissionInCache(permission);
  updateUrlPermissionInDB(permission);
}

function updateUrlPermissionInCache(permission) {
  const index = urlPermissionList.findIndex(
    (item) => item.url === permission.url
  );
  if (index >= 0) {
    const item = urlPermissionList[index];

    item.allowed = permission.allowed;
    item.url = permission.url;
    item.remember = permission.remember;
  }
}

function updateUrlPermissionInDB(permission) {
  openDatabase().then((db) => {
    const transaction = db.transaction(["url"], "readwrite");

    // report on the success of the transaction completing, when everything is done
    transaction.oncomplete = function (event) {
      console.debug("Url update transaction complete", event);
    };

    transaction.onerror = function (event) {
      console.error("Url update transaction error", event);
    };

    const objectStore = transaction.objectStore("url");

    const objectStoreRequest = objectStore.put(permission);

    objectStoreRequest.onsuccess = function (event) {
      console.debug("Url object store request update success", event);
    };

    objectStoreRequest.onerror = function (event) {
      console.error("Url object store request update error", event);
    };
  });
}

function loadGlobalSettings() {
  chrome.storage.sync.get(["setting"], function (result) {
    console.debug("Loaded global setting", result);
    globalSetting =
      Object.keys(result).length > 0 ? result : getDefaultSetting();
  });
}

// ---------------------------------------------------------------------------------

chrome.runtime.onInstalled.addListener(() => {
  console.debug(`Installing ${EXTENSION_NAME}...`);
  initGlobalSettings();
});

chrome.webRequest.onBeforeRequest.addListener(
  (details) => {
    console.debug("chrome.webRequest.onBeforeRequest.addListener", details);

    // extension turned off
    if (!isExtensionEnabled()) return;

    // checking only tabs
    if (!details.tabId) return;

    // if request allowed then live it alone
    if (isAllowedUrlRequest(details.url)) return;

    if (trackingRequestTypes.includes(details.type)) {
      // always checking URL
      const foundUrl = testContent(decodeURI(details.url), DATA_TYPE_URL);
      // using to transfer data in HTTP methods: POST, PUT, PATCH
      const foundRequestBody = testContent(
        JSON.stringify(details.requestBody),
        DATA_TYPE_BODY
      );

      const foundList = [].concat(foundUrl, foundRequestBody);
      if (foundList.length > 0) {
        const permission = findUrlRequestPermission(prepareUrl(details.url));

        // allowed request
        if (permission && permission.allowed) return;
        // deny request
        if (permission && !permission.allowed) return { cancel: true };

        askForRequestPermission(
          prepareUrl(details.url),
          foundList,
          details.tabId
        );

        // blocking request
        return { cancel: true };
      }
    }
  },
  { urls: ["<all_urls>"] },
  ["blocking", "requestBody"]
);

chrome.webRequest.onBeforeSendHeaders.addListener(
  (details) => {
    console.debug("chrome.webRequest.onBeforeSendHeaders.addListener", details);

    // extension turned off
    if (!isExtensionEnabled()) return;

    // checking only tabs
    if (!details.tabId) return;

    // if request allowed then live it alone
    if (isAllowedUrlRequest(details.url)) return;

    if (trackingRequestTypes.includes(details.type)) {
      const foundList = testContent(
        JSON.stringify(details.requestHeaders),
        DATA_TYPE_HEADERS
      );

      if (foundList.length > 0) {
        const permission = findUrlRequestPermission(prepareUrl(details.url));

        // allowed request
        if (permission && permission.allowed) return;
        // deny request
        if (permission && !permission.allowed) return { cancel: true };

        askForRequestPermission(
          prepareUrl(details.url),
          foundList,
          details.tabId
        );

        // blocking request
        return { cancel: true };
      }
    }
  },
  { urls: ["<all_urls>"] },
  ["blocking", "requestHeaders"]
);

// ---------------------------------------------------------------------------------

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  console.debug(
    "Received message from content script",
    message,
    sender,
    sendResponse
  );
  if (
    message.hasOwnProperty("type") &&
    message.type == "request_permission_action"
  ) {
    updateUrlPermission(message.data);
  }
});

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  console.debug(
    "Received message from content script",
    message,
    sender,
    sendResponse
  );
  if (message.hasOwnProperty("type") && message.type == "setting_changed") {
    loadGlobalSettings();
  }
});

// ---------------------------------------------------------------------------------

loadGlobalSettings();
loadUrlPermissionList();
