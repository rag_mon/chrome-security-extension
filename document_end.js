// function getFoundHumanName(name) {
//     switch (name) {
//         case 'ssn': return 'Social Security number';
//     }
// }

function getFoundListHtml(foundList) {
    let html = '<ul>';
    for (const found of foundList) {
        html += `<li>${found.name} in ${found.dataType}</li>`;
    }
    html += '</ul>';

    return html;
}

function getRequestUrlPermissionConfirmationModalHtml(url, foundList) {
    const foundListHtml = getFoundListHtml(foundList);

    return `
        <div class="custom-model-main model-open">
            <div class="custom-model-inner">        
                <!--<div class="close-btn">×</div>-->
                <div class="custom-model-wrap">
                    <div class="pop-up-content-wrap">
                        <div class="warning-icon">
                            <img src="${chrome.runtime.getURL('images/warning-icon.png')}">
                        </div>
                        <p>Data detected in request to ${url}</p>
                        ${foundListHtml}
                        <div>
                            <div>
                                <input type="checkbox" name="remember" class="remember">
                                <label>Remember action for this url</label>
                            </div>
                            <button type="button" class="btn_allow">Allow</button>
                            <button type="button" class="btn_deny">Deny</button>
                        </div>
                    </div>
                </div>
            </div>  
            <div class="bg-overlay"></div>
        </div> 
    `;
}

function createModal(url, foundList, allowCallback, denyCallback) {
    const element = document.createElement('div');

    element.innerHTML = getRequestUrlPermissionConfirmationModalHtml(url, foundList);

    const btnAllow = element.getElementsByClassName('btn_allow')[0];
    const btnDeny = element.getElementsByClassName('btn_deny')[0];

    function getDialogData(action) {
        const remember = element.getElementsByClassName('remember')[0];

        return {
            url: url,
            remember : remember.checked,
            allowed : action == 'allow' ? true : false
        };
    }

    btnAllow.addEventListener('click', function () {
        allowCallback(getDialogData('allow'));
        element.remove();
    });
    btnDeny.addEventListener('click', function () {
        denyCallback(getDialogData('deny'));
        element.remove();
    });
    
    return element;
}

function requestUrlPermissionDialog(url, foundList, allowCallback, denyCallback) {
    console.debug('requestUrlPermissionDialog', url, foundList, allowCallback, denyCallback);

    const modal = createModal(url, foundList, allowCallback, denyCallback);

    document.body.append(modal);

    // return new Promise((resolve, reject) => {
    //     // document.innerHTML = '';

    //     // const modal = createModal(
    //     //     (form) => {
    //     //         resolve('allow');
    //     //     },
    //     //     (form) => {
    //     //         resolve('deny');
    //     //     },
    //     //     () => {
    //     //         reject();
    //     //     }
    //     // );

    //     // modal.getElementById('allow_btn').onclick = function () {
    //     //     alert('hello');
    //     // };

    //     // document.body.appendChild(modal);
    //     // document.body.appendChild()

    //     // if (confirm('Confirm request?')) {
    //     //     resolve({
    //     //         allowed: true,
    //     //         remember: false
    //     //     });
    //     // } else {
    //     //     resolve({
    //     //         allowed: false,
    //     //         remember: false
    //     //     });
    //     // }
    // });
}

function sendRequestPermissionAction(data) {
    chrome.runtime.sendMessage({
        type: 'request_permission_action',
        data: data
    });
}


chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
    if (message.hasOwnProperty('type') && message.type == 'ask_for_request_permission') {
        console.debug('Received message from background script (ask_for_request_permission)', message, sender, sendResponse);
        // sendResponse('hello');
        requestUrlPermissionDialog(message.url, message.foundList, 
            (data) => {
                console.debug(`User confirm request for ${message.url}`, data, sendResponse);
                sendRequestPermissionAction(data);
            }, 
            (data) => {
                console.debug(`User decline request for ${message.url}`, data, sendResponse);
                sendRequestPermissionAction(data);
            }
        );
    }
});



// var script = document.createElement("script");
// script.src = chrome.runtime.getURL('document/injectable.js');
// script.onload = function () {
//     this.remove();
// };

// (document.head || document.documentElement).appendChild(script);


// const EXTENSION_ID = 'cdhcdmffjaemjpbfnhocghfmknilmldf';


// const DATA_TYPE_HEADERS = 'headers';
// const DATA_TYPE_BODY = 'body';
// const DATA_TYPE_URL = 'url';


// const MESSAGE_TYPE_GET_URL_PERMISSION = 'get_url_permission';


// const contentTests = {
//     'ssn' : function(content) {
//         // source: 078 05 1120 | 078-05-1120
//         return /(ssn)[ ="':\[\]\{\}\\]*(\d{3})[- +]?(\d{2})[- +]?(\d{4})/gmi.test(content);
//     },
// };

// // ---------------------------------------------------------------------------------

// function testContent(data, dataType) {
//     console.debug('Testing data', data, dataType);
//     for (const [testName, testFunc] of Object.entries(contentTests)) {

//         // if (testName in ) continue;

//         console.debug(`Running content test function "${testName}"...`);
//         if (testFunc(data)) {
//             console.debug(`Test function "${testName}" found matches`);
//             throw {
//                 dataType : dataType,
//                 found : testName
//             };
//         }
//     }
// }

// // ---------------------------------------------------------------------------------

// function getPermissionForUrl(url) {
//     console.log('getPermissionForUrl', url);
//     // chrome.extension.sendMessage({}, function(response) {
//     //     //code to initialize my extension
//     // });
//     // chrome.runtime.sendMessage({
//     //     type : MESSAGE_TYPE_GET_URL_PERMISSION,
//     //     url : url
//     // }, function (response) {
//     //     console.log('response from chrome.runtime.sendMessage', response);
//     // });
//     // chrome.runtime.sendMessage({type: 'get_url_permission'}, (response) => {
//     //     console.log('chrome.runtime.sendMessage response', response);
//     // });
//     // console.log('test');
//     // window.postMessage({
//     //     type : MESSAGE_TYPE_GET_URL_PERMISSION,
//     //     url : url
//     // });
// }

// function showActionPopup(found) {

// }

// function AjaxHookInternal() {
//     const self = this;

//     this.oldOpen = XMLHttpRequest.prototype.open;
//     this.oldSetRequestHeader = XMLHttpRequest.prototype.setRequestHeader;
//     this.oldSend = XMLHttpRequest.prototype.send;
//     this.last_url = null;

//     XMLHttpRequest.prototype.open = function (method, url, async, user, password) {
//         console.debug('XMLHttpRequest.prototype.open proxy call');
//         // getPermissionForUrl(url);
//         // try {
//         //     testContent(decodeURI(url), DATA_TYPE_URL);
//         // } catch (found) {
//         //     console.debug('Found some vulnerable data in URL', url, found);
//         // }

//         // self.last_url = url;

//         self.oldOpen(this, arguments);
//     };
//     // XMLHttpRequest.prototype.setRequestHeader = function () {
//     //     //
//     //     console.debug('XMLHttpRequest.prototype.setRequestHeader');
//     // };
// //     XMLHttpRequest.prototype.send = function (body) {
// //         console.debug('XMLHttpRequest.prototype.send proxy call', body);
// //         // var XMLHttpRequestContext = this;
// //         // setTimeout(function () {
// //         //     self.oldSend.apply(XMLHttpRequestContext, arguments);
// //         // }, 3000);
// //         // console.debug('body: ', body);
// //         // if (body === null) {
// //         // } else {
// //         //     throw 'Unexcpected body type: ' + (typeof body);
// //         // }

// //         // if () {
// // // 
// //         // }

// //         try {
// //             testContent(JSON.stringify(body), DATA_TYPE_BODY);
// //         } catch (found) {
// //             console.debug('Found some vulnerable data in requestBody', body, found);
// //             //
// //         }
// //     };
// }


// // ---------------------------------------------------------------------------------


// // const chromeRuntimePort = chrome.runtime.connect(EXTENSION_ID);

// // window.addEventListener("message", function(event) {
// //     console.log('window.addEventListener', event);
// //     // We only accept messages from ourselves
// //     if (event.source != window)
// //         return;

// // //   if (event.data.type && (event.data.type == "FROM_PAGE")) {
// // //     console.log("Content script received: " + event.data.text);
// // //     chromeRuntimePort.postMessage(event.data.text);
// // //   }
// //     chrome.runtime.sendMessage(EXTENSION_ID, event.data);
// // }, false);




// var ajaxHookInternal = new AjaxHookInternal();
// console.debug('Internal AJAX hook installed ....');